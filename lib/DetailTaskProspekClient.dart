import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:martools_tracker_mockup/fab.dart';
import 'package:intl/intl.dart';
import 'bottomFormButton.dart';
import 'data.dart';
import 'util.dart';

class DetailTaskProspekClient extends StatefulWidget {
  DetailTaskProspekClient({Key key}) : super(key: key);

  _DetailTaskProspekClientState createState() => _DetailTaskProspekClientState();
}

class _DetailTaskProspekClientState extends State<DetailTaskProspekClient> {
  var data;
  bool autoValidate = true;
  bool readOnly = false;
  bool showSegmentedControl = true;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final GlobalKey<FormFieldState> _specifyTextFieldKey =
  GlobalKey<FormFieldState>();
  final bottomNavigationBarIndex = 1;

  ValueChanged _onChanged = (val) => print(val);

  @override
  void initState() {
    super.initState();
    final _selectedDay = DateTime.now();

  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: GradientAppBar(
          gradient:  LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [CustomColors.HeaderBlueDark, CustomColors.HeaderBlueLight],
          ),
          title: Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Visit Pertamina',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                ),
                Text(
                  'penawaran asuransi kendaraan',
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
          bottom: TabBar(tabs: <Widget>[
            Tab(
              text: 'Staff Detail',
            ),
            Tab(
              text: 'Log History',
            )
          ]),
        ),
        body: TabBarView(
          children: [
            Scaffold(
              body: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      FormBuilder(
                        // context,
                        key: _fbKey,
                        autovalidate: true,
                        initialValue: {
                          'movie_rating': 5,
                        },
                        // readOnly: true,
                        child: Column(
                          children: <Widget>[
                            FormBuilderTextField(
                              attribute: "marketing_name",
                              decoration: InputDecoration(
                                labelText: "Marketing Name",
                                /*border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                        ),*/
                              ),
                              onChanged: _onChanged,
                              valueTransformer: (text) => num.tryParse(text),
                              validators: [
                                FormBuilderValidators.max(70),
                              ],
                              initialValue: "Grace Natalie",
                              readOnly: true,
                            ),
                            FormBuilderTextField(
                              attribute: "phone_number",
                              decoration: InputDecoration(
                                labelText: "Phone Number",
                                /*border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                        ),*/
                              ),
                              onChanged: _onChanged,
                              valueTransformer: (text) => num.tryParse(text),
                              validators: [
                                FormBuilderValidators.max(70),
                              ],
                              initialValue: "082211323232",
                              readOnly: true,
                            ),
                            FormBuilderTextField(
                              attribute: "email",
                              decoration: InputDecoration(
                                labelText: "email",
                                /*border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                        ),*/
                              ),
                              onChanged: _onChanged,
                              valueTransformer: (text) => num.tryParse(text),
                              validators: [
                                FormBuilderValidators.max(70),
                              ],
                              initialValue: "grace@maroon.com",
                              readOnly: true,
                            ),
                            FormBuilderTextField(
                              attribute: "position",
                              decoration: InputDecoration(
                                labelText: "Position",
                                /*border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red),
                        ),*/
                              ),
                              onChanged: _onChanged,
                              valueTransformer: (text) => num.tryParse(text),
                              validators: [
                                FormBuilderValidators.max(70),
                              ],
                              initialValue: "Senior Sales",
                              readOnly: true,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
//              bottomNavigationBar:
//              BottomFormButton(),
            ),
            Scaffold(
              body: Container(
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                        padding: EdgeInsets.fromLTRB(5, 13, 5, 13),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset('assets/images/checked.png'),
                            Text(
                              '2019-11-21',
                              style: TextStyle(color: CustomColors.TextGrey),
                            ),
                            Container(
                              width: 180,
                              child: Text(
                                'Visit perdana, perkenalan produk',
                                style: TextStyle(
                                    color: CustomColors.TextHeader,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [CustomColors.PurpleIcon, Colors.white],
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: CustomColors.GreyBorder,
                              blurRadius: 10.0,
                              spreadRadius: 5.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                      ),
//                      onTap: () => Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => DetailTaskProspekPerdana()),
//                      ),
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                        padding: EdgeInsets.fromLTRB(5, 13, 5, 13),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset('assets/images/checked.png'),
                            Text(
                              '2019-11-23',
                              style: TextStyle(color: CustomColors.TextGrey),
                            ),
                            Container(
                              width: 180,
                              child: Text(
                                'Visit kedua, pitching ke direksi',
                                style: TextStyle(
                                    color: CustomColors.TextHeader,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [CustomColors.PurpleIcon, Colors.white],
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: CustomColors.GreyBorder,
                              blurRadius: 10.0,
                              spreadRadius: 5.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                      ),
//                      onTap: () => Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => DetailTaskProspekPerdana()),
//                      ),
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                        padding: EdgeInsets.fromLTRB(5, 13, 5, 13),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset('assets/images/checked.png'),
                            Text(
                              '2019-11-25',
                              style: TextStyle(color: CustomColors.TextGrey),
                            ),
                            Container(
                              width: 180,
                              child: Text(
                                'Visit ketiga, persiapan berkas-berkas',
                                style: TextStyle(
                                    color: CustomColors.TextHeader,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [CustomColors.PurpleIcon, Colors.white],
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: CustomColors.GreyBorder,
                              blurRadius: 10.0,
                              spreadRadius: 5.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                      ),
//                      onTap: () => Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => DetailTaskProspekPerdana()),
//                      ),
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                        padding: EdgeInsets.fromLTRB(5, 13, 5, 13),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset('assets/images/checked.png'),
                            Text(
                              '2019-11-27',
                              style: TextStyle(color: CustomColors.TextGrey),
                            ),
                            Container(
                              width: 180,
                              child: Text(
                                'Tanda tangan kontrak kerjasama',
                                style: TextStyle(
                                    color: CustomColors.TextHeader,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            stops: [0.015, 0.015],
                            colors: [CustomColors.PurpleIcon, Colors.white],
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: CustomColors.GreyBorder,
                              blurRadius: 10.0,
                              spreadRadius: 5.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ],
                        ),
                      ),
//                      onTap: () => Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => DetailTaskProspekPerdana()),
//                      ),
                    ),
                    SizedBox(height: 15)
                  ],
                ),
              ),
              floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//              floatingActionButton: customFabDetailProspek(context),
            )
          ],
        ),
      ),
    );
  }

}
